import path from 'path';

import { Application } from 'express';
import swaggerJSDoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';

export const initSwagger = (app: Application) => {
  const swaggerDefinition = {
    swagger: '2.0',

    info: {
      title: 'Express API for JSONPlaceholder',
      version: '1.0.0',
      description: 'This is a REST API application made with Express. It retrieves data from JSONPlaceholder.',
      license: {
        name: 'Licensed Under MIT',
        url: 'https://spdx.org/licenses/MIT.html',
      },
      contact: {
        name: 'JSONPlaceholder',
        url: 'https://jsonplaceholder.typicode.com',
      },
    },
    host: `localhost:${process.env.PORT || 4000}`,
    basePath: '/v1',
    servers: [
      {
        url: `http://localhost:${process.env.PORT || 4000}/v1`,
        description: 'Development server',
      },
    ],
  };

  const options = {
    swaggerDefinition,
    // Paths to files containing OpenAPI definitions
    apis: [path.join(`${__dirname}`, '..', '/routes/v1/*/*.ts')],
  };

  const swaggerSpec = swaggerJSDoc(options);

  app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec, { explorer: true }));
};

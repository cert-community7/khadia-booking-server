declare namespace Express {
  export interface Request {
    db: import('@entities').DbEntities;
    userId: string
  }
}

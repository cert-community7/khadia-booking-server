import { createJwtToken } from '@utils/jwtToken';
import { Request } from 'express';
import { StatusCodes } from 'http-status-codes';
import { generateControllerError } from 'src/utils/communication';

const LoginControllerType = {
  email: String,
  password: String,
}

const loginController = async (req: Request) => {
  const {  email , password} = req.body
  const db = req.db
  const user = await db.User.findOne({
    where: {
      email: email,
      password: password,
      canLogin: true,
    }
  })
  if(user){
    try{
      const jwtString = createJwtToken({
      id: user.id,
      email: user.email
      })
      return  { user, token: jwtString }
    }catch(jwtError){
      throw generateControllerError({
        error: "Cannot create jwt token.",
        errorCode: StatusCodes.INTERNAL_SERVER_ERROR
      })
    }
  }else{
    throw generateControllerError({
      error: 'User not found',
      errorCode: StatusCodes.NOT_FOUND,
    });
  }
};

export { loginController };

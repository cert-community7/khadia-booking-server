import { createJwtToken } from '@utils/jwtToken';
import { Request } from 'express';
import { StatusCodes } from 'http-status-codes';
import { generateControllerError } from 'src/utils/communication';


const getCurrentUserController = async (req: Request) => {
 
  const { userId } = req
  console.log(8888 , userId)
  const db = req.db
  const user = await db.User.findOne({
    where: {
      id: userId
    }
  })
  if(user){
    return {user}
  }else{
    throw generateControllerError({
      error: 'User not found',
      errorCode: StatusCodes.NOT_FOUND,
    });
  }
};

export {
    getCurrentUserController
};

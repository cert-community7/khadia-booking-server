import User from './user';

const allEntries = {
    User
}

export type DbEntities = typeof allEntries

export default allEntries
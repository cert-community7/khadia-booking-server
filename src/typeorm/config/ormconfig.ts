import { ConnectionOptions } from 'typeorm';

import { isDevEnvironment } from '../../utils/envUtils';

const envBasedSettings = isDevEnvironment()
  ? {}
  : {
      ssl: true,
      extra: {
        ssl: {
          rejectUnauthorized: false,
        },
      },
    };

const config: ConnectionOptions = {
  type: 'postgres',
  host: process.env.PG_HOST,
  port: Number(process.env.PG_PORT),
  username: process.env.POSTGRES_USER,
  password: process.env.POSTGRES_PASSWORD,
  database: process.env.POSTGRES_DB,
  synchronize: true,
  logging: false,
  entities: ['src/typeorm/entities/**/*.ts'],
  ...envBasedSettings,
  // migrations: ['src/typeorm/migrations/**/*.ts'],
  // subscribers: ['src/typeorm/subscriber/**/*.ts'],
  // cli: {
  //   entitiesDir: 'src/typeorm/entities',
  //   migrationsDir: 'src/typeorm/migrations',
  //   subscribersDir: 'src/typeorm/subscriber',
  //},
  //namingStrategy: new SnakeNamingStrategy(),
};

export = config;

import jwt from 'jsonwebtoken';

export function createJwtToken<T extends Object>(payload: T): string {
  return jwt.sign(payload, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRATION,
  });
};

export function verifyJwtToken(token): Promise<any>{
  return new Promise((res , rej) => {
    return jwt.verify(token , process.env.JWT_SECRET, (err , data) => {
      if (err){
        return rej(err)
      }
      return res(data)
    })
  })
}
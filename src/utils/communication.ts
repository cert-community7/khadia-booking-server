import { Response } from 'express';

type customResponse = {
  res: Response;
  message: string;
};

type successMessage = customResponse & {
  successCode: number;
  data: Record<string, unknown>;
};

type errorMessage = {
  errorCode: number;
  error: Record<string, unknown> | Error | string;
};

type errorResponse = errorMessage & customResponse;

export const generateSuccessResponse = ({ res, successCode, message, data }: successMessage) => {
  return res.status(successCode).json({
    status: successCode,
    message,
    data,
    error: null,
  });
};

export const generateErrorResponse = ({ res, errorCode, message, error }: errorResponse) => {
  error = String(error || '')
  return res.status(errorCode).json({
    status: errorCode,
    message,
    data: null,
    errorSimplified: error.toString(),
    error,
  });
};

export const generateControllerError = ({ errorCode, error }: errorMessage) => {
  return { errorCode, error };
};

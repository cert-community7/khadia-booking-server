import otherRouters from './others';
import v1Router from './v1';

const routes = (app) => {
  app.use('/v1', v1Router);
  app.use(otherRouters);
};

export default routes;

import {loginController} from '@controllers/login';
import { getCurrentUserController } from '@controllers/user'
import { schemaValidation } from '@middlewares';
import { jwtVerification } from '@middlewares/jwtVerification';
import { generateErrorResponse, generateSuccessResponse } from '@utils/communication';
import { login } from '@validations/user';
import { Router, Request, Response } from 'express';
import { StatusCodes } from 'http-status-codes';

const userRouter = Router();

/**
 * @swagger
 * /users/login:
 *   post:
 *     summary: Get a JWT token for a user.
 *     description: Get JWT token by providing email and password.
 *     produces:
 *       - application/json
 *     consumes: 
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: login
 *         description: email and password for login
 *         schema:
 *           type: object
 *           properties:
 *             email:
 *               type: string
 *               example: admin@admin.com
 *             password:
 *               type: string
 *               example: admin
 *     responses:
 *       400:
 *           description: Bad Request.
 *           schema:
 *             type: object
 *             properties:
 *               data:
 *                 type: null
 *                 example: null
 *               message:
 *                  type: string
 *                  example: Bad Request
 *               status:
 *                   type: number
 *                   example: 400
 *               error:
 *                   type: object
 *       200:
 *         description: A valid jwt token.
 *         schema:
 *           type: object
 *           properties:
 *             data:
 *               type: object
 *               properties:
 *                 token:
 *                   type: string
 *                   description: JWT token
 *             status:
 *                type: number
 *                example: 200
 *             error:
 *                 type: null
 *                 example: null
 *             message:
 *                 type: string
 *                 description: "Login Successful Message"
 *                 example: "Login Successful"
 */
userRouter.post('/login', schemaValidation(login), async (req: Request, res: Response) => {
  try {
    const data = await loginController(req);
    return generateSuccessResponse({ res, successCode: StatusCodes.OK, message: 'User loggedin successfully.', data });
  } catch (errorContainer) {
    const { errorCode, error } = errorContainer;
    console.log(error);
    return generateErrorResponse({ res, errorCode, error, message: 'User cannot login.' });
  }
});

/**
 * @swagger
 * /users/getCurrentUser:
 *   get:
 *     summary: Get details of a current user.
 *     description: Get details of a current user by providing only jwt token.
 *     produces:
 *       - application/json
 *     consumes: 
 *       - application/json
 *     parameters:
 *       - name: authorization
 *         in: header
 *         required: true
 *         type: string
 *         description: an authorization header
 *     responses:
 *       400:
 *           description: Bad Request.
 *           schema:
 *             type: object
 *             properties:
 *               data:
 *                 type: null
 *                 example: null
 *               message:
 *                 type: string
 *                 example: Bad Request
 *               status:
 *                 type: number
 *                 example: 400
 *               error:
 *                 type: object
 *       200:
 *         description: A valid current user.
 *         schema:
 *           type: object
 *           properties:
 *             data:
 *               type: object
 *               properties:
 *                 user:
 *                   type: object
 *                   description: User object
 *             status:
 *                type: number
 *                example: 200
 *             error:
 *                 type: null
 *                 example: null
 *             message:
 *                 type: string
 *                 description: "User details fetched"
 *                 example: "User details fetched"
 */
userRouter.get('/getCurrentUser', jwtVerification() , async (req: Request, res: Response) => {
  try {
    const data = await getCurrentUserController(req);
    return generateSuccessResponse({ res, successCode: StatusCodes.OK, message: 'Details of current user fetched successfully.', data });
  } catch (errorContainer) {
    const { errorCode, error } = errorContainer;
    console.log(error);
    return generateErrorResponse({ res, errorCode, error, message: 'Cannot find details of current user.' });
  }
});

export default userRouter;

import { Router } from 'express';

const noFoundRoute = Router();

noFoundRoute.get('*', (req, res) => {
  res.json({
    message: 'Route Not Found',
  });
});

export default noFoundRoute;

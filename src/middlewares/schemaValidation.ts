import { Request, Response, NextFunction } from 'express';
import { StatusCodes } from 'http-status-codes';
import Joi from 'joi';
import { generateErrorResponse } from 'src/utils/communication';

export const schemaValidation = (shcemaType: Joi.Schema) => {
  return async (req: Request, res: Response, next: NextFunction) => {
    {
      let dataToValidate;
      if (req.method === 'OPTION' || req.method === "OPTIONS") {
        return next();
      } else if (req.method === 'GET') {
        dataToValidate = req.query;
      } else {
        dataToValidate = req.body;
      }
      try {
        await shcemaType.validateAsync(dataToValidate, {
          abortEarly: true,
          allowUnknown: true,
          stripUnknown: true,
        });
        return next();
      } catch (error) {
        return generateErrorResponse({
          res,
          errorCode: StatusCodes.BAD_REQUEST,
          message: 'Schema validation failed, bad request.',
          error,
        });
      }
    }
  };
};

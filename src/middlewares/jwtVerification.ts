import { verifyJwtToken } from '@utils/jwtToken';
import { Request, Response, NextFunction } from 'express';
import { StatusCodes } from 'http-status-codes';
import { generateErrorResponse } from 'src/utils/communication';

export const jwtVerification =  () => async (req: Request, res: Response, next: NextFunction) => {
    try{
        const authorizationHeader = req.header('authorization');
        if (authorizationHeader){
            try{
                const jwtData = await verifyJwtToken(authorizationHeader);
                console.log(jwtData , 1222)
                req.userId = jwtData.id;
                return next();
            }catch(error){
                return generateErrorResponse({
                    res,
                    errorCode: StatusCodes.UNAUTHORIZED,
                    error,
                    message: "Jwt token might be corrupt or expired"
                })
            }
        }else{
            return generateErrorResponse({
                res,
                errorCode: StatusCodes.BAD_REQUEST,
                error: "Authorization header is not present in the request.",
                message: "Missing authorization header"
            })
        }

        return next();
      } catch (error) {
        return generateErrorResponse({
          res,
          errorCode: StatusCodes.INTERNAL_SERVER_ERROR,
          message: 'DB connection mapping failed.',
          error,
        });
      }
    
}

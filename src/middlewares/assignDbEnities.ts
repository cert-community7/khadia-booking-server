import { Request, Response, NextFunction } from 'express';
import { StatusCodes } from 'http-status-codes';
import dbEntities from '@entities'
import { generateErrorResponse } from 'src/utils/communication';

export const assignDbEntities =  () => (req: Request, res: Response, next: NextFunction) => {
    try{
        req.db = dbEntities;
        return next();
      } catch (error) {
        return generateErrorResponse({
          res,
          errorCode: StatusCodes.INTERNAL_SERVER_ERROR,
          message: 'DB connection mapping failed.',
          error,
        });
      }
    
}

import 'dotenv/config';
import 'reflect-metadata';
//import fs from 'fs';
//import path from 'path';

import bodyParser from 'body-parser';
//import cors from 'cors';
import express from 'express';
import helmet from 'helmet';
import morgan from 'morgan';

import { initSwagger } from './config/swagger';
import routes from './routes';
import { dbCreateConnection } from './typeorm/dbCreateConnection';
import { assignDbEntities } from '@middlewares';
import { StatusCodes } from 'http-status-codes';

export const app = express();

//app.use(cors());
app.use((req , res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', 'true');
  if(['OPTION' , 'OPTIONS'].includes(req.method)){
    return res.sendStatus(StatusCodes.OK)
  }
  return next();
})
app.use(helmet());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(assignDbEntities())

// try {
//   const accessLogStream = fs.createWriteStream(path.join(__dirname, '../log/access.log'), {
//     flags: 'a',
//   });
//   app.use(morgan('combined', { stream: accessLogStream }));
// } catch (err) {
//   console.log(err);
// }
app.use(morgan('combined'));

initSwagger(app);
routes(app);

const port = process.env.PORT || 4000;
app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});

(async () => {
  await dbCreateConnection();
})();
